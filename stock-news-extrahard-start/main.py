from datetime import datetime
from pprint import pprint
import requests
from lxml import html
from twilio.rest import Client
import math

STOCK = "TSLA"
COMPANY_NAME = "Tesla Inc"
ALPHA_VANTAGE_API_KEY = "OW7RFGDS3OM3KZ1K"
ALPHA_VANTAGE_HOST = "https://www.alphavantage.co/query"
NEWSAPI_API_KEY = "65e0d6c363c9463fb1712ffc66c7b129"
NEWSAPI_HOST = "https://newsapi.org/v2/everything"
TWILIO_ACCOUNT_SID = 'AC7d02f87b44eccbbb224e80185b92c74a'
TWILIO_AUTH_TOKEN = '8a606e22dd6c10feca1336bef781a9a5'

def retrieve_news() -> [()]:
    parameters2 = {
        "q": COMPANY_NAME,
        "from": today,
        "sortBy": "popularity",
        "apiKey": NEWSAPI_API_KEY,
    }
    response2 = requests.get(NEWSAPI_HOST, params=parameters2)
    response2.raise_for_status()
    articles = response2.json()['articles'][:3]
    return [(article['title'], article['description']) for article in articles]

def get_percentage_difference(v1: float, v2: float):
    return math.floor((v1 - v2) / ((v1+v2) / 2) * 100 * 100) / 100.0


# STEP 1: Use https://www.alphavantage.co
parameters = {
    "function": "TIME_SERIES_DAILY",
    "symbol": STOCK,
    "outputsize": "compact",
    "apikey": ALPHA_VANTAGE_API_KEY,
}
response = requests.get(ALPHA_VANTAGE_HOST, params=parameters)
response.raise_for_status()
response_body = response.json()
pprint(response_body)

# today = datetime.today()
today = datetime(year=2021, month=12, day=23)
today_formatted = f'{today.year}-{today.month}-{today.day}'

try:
    new_data = response_body['Time Series (Daily)'][today_formatted]
    open_price = float(response_body['Time Series (Daily)'][today_formatted]['1. open'])
    close_price = float(response_body['Time Series (Daily)'][today_formatted]['4. close'])
# {'1. open': '965.66', '2. high': '1015.6599', '3. low': '957.05', '4. close': '1008.87', '5. volume': '31211362'}
except KeyError as e:
    print('no data found for this day', e)
else:
    percent_difference = get_percentage_difference(open_price, close_price)
    # STEP 2: Use https://newsapi.org
    # Instead of printing ("Get News"), actually get the first 3 news pieces for the COMPANY_NAME.
    if math.fabs(percent_difference) > 5:
        tuple2_list = retrieve_news()

        # STEP 3: Use https://www.twilio.com
        # Send a seperate message with the percentage change and each article's title and description to your phone number.
        def send_sms(body: str):
            client = Client(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN)
            message = client.messages.create(
                body=body,
                from_='+13028657262',
                to='+32497570403'
            )
        tmp_emoji = '📉'
        if percent_difference > 0:
            tmp_emoji = '📈'
        body = f'\n{COMPANY_NAME} {tmp_emoji} {percent_difference}%\n'
        for t in tuple2_list:
            body += str(html.fromstring(f'Headline: {t[0]}\n').text_content())
            body += str(html.fromstring(f'Brief: {t[1]}\n').text_content())
        print('--- THE FOLLOWING TEXT/SMS WILL BE SENT ---\n', body)
        # send_sms(body)

# Optional: Format the SMS message like this:

"""
TSLA: 🔺2%
Headline: Were Hedge Funds Right About Piling Into Tesla Inc. (TSLA)?. 
Brief: We at Insider Monkey have gone over 821 13F filings that hedge funds and prominent investors are required to file by the SEC The 13F filings show the funds' and investors' portfolio positions as of March 31st, near the height of the coronavirus market crash.
or
"TSLA: 🔻5%
Headline: Were Hedge Funds Right About Piling Into Tesla Inc. (TSLA)?. 
Brief: We at Insider Monkey have gone over 821 13F filings that hedge funds and prominent investors are required to file by the SEC The 13F filings show the funds' and investors' portfolio positions as of March 31st, near the height of the coronavirus market crash.
"""
